import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";
import ProjectDetail from "../views/ProjectDetail.vue";
import CreateProject from "../views/CreateProject.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
    },
    {
      path: "/project/create",
      name: "create-project",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/CreateProject.vue"),
      // component: CreateProject,
    },
    {
      path: "/project/:id",
      name: "project",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/ProjectDetail.vue"),
      // component: ProjectDetail,
    },
    {
      path: "/lifecycleModels",
      name: "lifeCycleModels",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/LifecycleView.vue"),
      // component: ProjectDetail,
    },
    {
      path: "/actions",
      name: "actions",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/Actions.vue"),
      // component: ProjectDetail,
    },
  ],
});

export default router;
