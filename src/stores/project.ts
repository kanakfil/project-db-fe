import { defineStore } from "pinia";

import axios from "axios";
import type CreateRequest from "@/model/CreateRequest";
import type Project from "@/model/Project";
import { getCurrentInstance, inject } from "vue";
import ProjectService from "@/service/ProjectService";
import { toastBus } from "@/main";
import type { ProjectUpdateAction } from "@/model/Action";
import type { LifecycleModel } from "@/model/Project";

const SERVER_URL = import.meta.env["VITE_SERVER_URL"];
const client = axios.create({ baseURL: SERVER_URL + "/api" });

const projectService = new ProjectService();

export type ProjectState = {
  projects: Project[];
  project: Project | null;
  loading: boolean;
  error: unknown;
  projectTypes: string[];
  lifecycleModels: LifecycleModel[];
};

export const useProjectStore = defineStore({
  id: "project",
  state: () =>
    ({
      projects: [],
      project: null,
      loading: false,
      error: null,
      projectTypes: [],
      lifecycleModels: [],
    } as ProjectState),
  getters: {
    lifecycleModelNames: (state) => state.lifecycleModels.map((m) => m.name),
  },
  actions: {
    async fetchProjects(token: string) {
      this.projects = [];
      this.loading = true;
      try {
        await client
          .get("/projects", {
            headers: {
              Authorization: "Bearer " + token,
            },
          })
          .then(
            (response) =>
              (this.projects = response.data.map((project: any) =>
                projectService.getProject(project)
              ))
          );
      } catch (error: any) {
        toastBus.emit("toast", {
          severity: "error",
          summary: error.name,
          detail: error.message,
        });
      } finally {
        this.loading = false;
      }
    },
    async createProject(createRequest: CreateRequest, token: string) {
      this.loading = true;
      return client
        .post("/projects", createRequest, {
          headers: {
            Authorization: "Bearer " + token,
          },
        })
        .then((response) => {
          this.project = response.data;
        })
        .catch((err) => (this.error = err))
        .then(() => {
          this.loading = false;
        });
    },
    async getProject(id: number, token: string) {
      if (this.project?.id !== id) {
        return client
          .get("/projects/" + id, {
            headers: {
              Authorization: "Bearer " + token,
            },
          })
          .then((response) => {
            return (this.project =
              projectService.getProject(response.data) ?? null);
          });
      }
    },
    async updateProject(action: ProjectUpdateAction) {
      this.loading = true;
      return client
        .put("/projects/" + action.project.id, action, {
          headers: {
            Authorization: "Bearer " + action.token,
          },
        })
        .then((response) => {
          return (this.project =
            projectService.getProject(response.data) ?? null);
        })
        .catch((err) => (this.error = err))
        .then(() => {
          this.loading = false;
        });
    },
    async getProjectTypes(token: string) {
      return client
        .get("/projects/types", {
          headers: {
            Authorization: "Bearer " + token,
          },
        })
        .then((response) => {
          return (this.projectTypes = response.data);
        });
    },
    async getProjectModels(token: string) {
      return client
        .get("/projects/lifecycleModels", {
          headers: {
            Authorization: "Bearer " + token,
          },
        })
        .then((response) => {
          return (this.lifecycleModels = response.data);
        });
    },
  },
});
