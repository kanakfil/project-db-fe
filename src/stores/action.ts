import { defineStore } from "pinia";

import axios from "axios";
import type { LifecycleModel } from "@/model/Project";
import StatusUpdateAction from "@/model/StatusUpdateAction";
import { ModuleAction } from "@/model/StatusUpdateAction";
import { toastBus } from "@/main";

const SERVER_URL = import.meta.env["VITE_SERVER_URL"];
const client = axios.create({ baseURL: SERVER_URL + "/api" });

export type ActionState = {
  actions: StatusUpdateAction[];
  loading: boolean;
  action: StatusUpdateAction | null;
  error: unknown;
  lifecycleModels: LifecycleModel[];
  available: ModuleAction[];
  actionDialog: boolean;
  deleteActionDialog: boolean;
  submitted: boolean;
};

export const useActionStore = defineStore({
  id: "action",
  state: () =>
    ({
      actions: [],
      loading: false,
      action: null,
      error: null,
      lifecycleModels: [],
      available: [],
      actionDialog: false,
      deleteActionDialog: false,
      submitted: false,
    } as ActionState),
  getters: {
    statusDropdownData: (state) => {
      return state.lifecycleModels.map((m) => {
        return {
          name: m.name,
          statuses: m.projectStatuses.map((s) => ({
            name: s,
          })),
        };
      });
    },
  },
  actions: {
    async getLifecycleModels(token: string) {
      return client
        .get("/projects/lifecycleModels", {
          headers: {
            Authorization: "Bearer " + token,
          },
        })
        .then((response) => {
          return (this.lifecycleModels = response.data);
        });
    },
    async getActions(token: string) {
      return client
        .get("/actions", {
          headers: {
            Authorization: "Bearer " + token,
          },
        })
        .then((response) => {
          return (this.actions = response.data.map(
            (r: any) => new StatusUpdateAction(r.from, r.to, r.moduleActions)
          ));
        });
    },
    async getAvailableActions(token: string) {
      return client
        .get("/actions/available", {
          headers: {
            Authorization: "Bearer " + token,
          },
        })
        .then((response) => {
          return (this.available = response.data.map(
            (r: any) => new ModuleAction(r.module, r.action)
          ));
        });
    },
    async createAction(action: StatusUpdateAction, token: string) {
      return client
        .post("/actions", action, {
          headers: {
            Authorization: "Bearer " + token,
          },
        })
        .then((response) => {
          this.actionDialog = false;
          return (this.actions = response.data.map(
            (r: any) => new StatusUpdateAction(r.from, r.to, r.moduleActions)
          ));
        });
    },
    async deleteAction(action: StatusUpdateAction, token: string) {
      return client
        .delete("/actions", {
          params: {
            from: action.from,
            to: action.to,
          },
          headers: {
            Authorization: "Bearer " + token,
          },
        })
        .then((response) => {
          return (this.actions = response.data.map(
            (r: any) => new StatusUpdateAction(r.from, r.to, r.moduleActions)
          ));
        });
    },
  },
});
