export default class Utils {
  /**
   * credits https://github.com/TimPietrusky/randomstringtocsscolor
   * @param name
   */
  public computeColor(name: string) {
    let value: string | string[] = name,
      result: string | string[] = "";

    value = value.split("");

    // Filter non hex values
    for (let i = 0; i < value.length; i++) {
      let val: number | string = value[i];

      if (!/^[0-9A-F]{1}$/i.test(val)) {
        val = 0;
      }

      result += val;
    }

    // Multiple of 3
    if (result.length % 3) {
      result += Array(3 - (result.length % 3) + 1).join("0");
    }

    // Multiple of 6
    if (result.length % 6) {
      // result += Array((6 - result.length % 6) + 1).join("0");
    }

    // Split in 3 groups with equal size
    const regexp = new RegExp("([A-Z0-9]{" + result.length / 3 + "})", "i");
    result = result.split(regexp);

    // Remove first 0 (if there is one at first postion of every group
    if (result[1].length > 2) {
      if (
        result[1].charAt(0) == "0" &&
        result[3].charAt(0) == "0" &&
        result[5].charAt(0) == "0"
      ) {
        result[1] = result[1].substr(1);
        result[3] = result[3].substr(1);
        result[5] = result[5].substr(1);
      }
    }

    // Truncate (first 2 chars stay, the rest gets deleted)
    result[1] = result[1].slice(0, 2);
    result[3] = result[3].slice(0, 2);
    result[5] = result[5].slice(0, 2);

    // Add element if color consists of just 1 char per color
    if (result[1].length == 1) {
      result[1] += result[1];
      result[3] += result[3];
      result[5] += result[5];
    }

    // Output
    return "#" + result[1] + result[3] + result[5];
  }

  private getRGB(c: string): number {
    return parseInt(c, 16) || 0;
  }

  private getsRGB(c: string) {
    return this.getRGB(c) / 255 <= 0.03928
      ? this.getRGB(c) / 255 / 12.92
      : Math.pow((this.getRGB(c) / 255 + 0.055) / 1.055, 2.4);
  }

  private getLuminance(hexColor: string) {
    return (
      0.2126 * this.getsRGB(hexColor.substr(1, 2)) +
      0.7152 * this.getsRGB(hexColor.substr(3, 2)) +
      0.0722 * this.getsRGB(hexColor.substr(-2))
    );
  }

  private getContrast(f: string, b: string) {
    const L1 = this.getLuminance(f);
    const L2 = this.getLuminance(b);
    return (Math.max(L1, L2) + 0.05) / (Math.min(L1, L2) + 0.05);
  }

  /**
   * credits https://wunnle.com/dynamic-text-color-based-on-background
   * @param bgColor
   */
  public getTextColor(bgColor: string) {
    const whiteContrast = this.getContrast(bgColor, "#ffffff");
    const blackContrast = this.getContrast(bgColor, "#000000");

    return whiteContrast > blackContrast ? "#ffffff" : "#000000";
  }
}
