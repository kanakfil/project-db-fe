import Project, { LifecycleModel } from "@/model/Project";

export default class ProjectService {
  getProject(response: any): Project {
    return new Project(
      response.id,
      response.name,
      response.statusHistory,
      response.type.name,
      new LifecycleModel(response.model.name, response.model.projectStatuses),
      []
    );
  }
}
