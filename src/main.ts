import { createApp } from "vue";
import { createPinia } from "pinia";

import App from "./App.vue";
import router from "./router";
import type Keycloak from "keycloak-js";
import VueKeyCloakJs from "@dsb-norge/vue-keycloak-js";
import type { VueKeycloakInstance } from "@dsb-norge/vue-keycloak-js/dist/types";
import PrimeVue from "primevue/config";
import Menubar from "primevue/menubar";

import "primevue/resources/themes/saga-blue/theme.css";
import "primevue/resources/primevue.min.css";
import "primeicons/primeicons.css";
import "@/assets/primeflex/primeflex.scss";
import InputText from "primevue/inputtext";
import Ripple from "primevue/ripple";
import DataView from "primevue/dataview";
import Timeline from "primevue/timeline";
import ToastService from "primevue/toastservice";
import mitt from "mitt";
import type { Events } from "@/model/Toast";
import Chart from "primevue/chart";

const toastBus = mitt<Events>();

const app = createApp(App);

app.provide("toastBus", toastBus);

app.use(VueKeyCloakJs, {
  init: {
    onLoad: "check-sso",
    silentCheckSsoRedirectUri:
      window.location.origin + "/silent-check-sso.html",
    checkLoginIframe: true,
  },
  config: {
    url: "http://localhost:8081",
    realm: "ProjectDB",
    clientId: "projectdb-fe",
  },
  onReady(keycloak: Keycloak) {
    console.log("Keycloak ready", keycloak);
  },
});

app.use(PrimeVue, { ripple: true });
app.use(ToastService);

app.directive("ripple", Ripple);

// eslint-disable-next-line vue/multi-word-component-names
app.component("Menubar", Menubar);
app.component("InputText", InputText);
app.component("DataView", DataView);
// eslint-disable-next-line vue/multi-word-component-names
app.component("Timeline", Timeline);
// eslint-disable-next-line vue/multi-word-component-names
app.component("Chart", Chart);

app.use(createPinia());
app.use(router);

app.mount("#app");

declare module "@vue/runtime-core" {
  interface ComponentCustomProperties {
    $keycloak: VueKeycloakInstance;
  }
}

export { toastBus };
