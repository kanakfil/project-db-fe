export type Toast = {
  severity: string;
  summary: string;
  detail: string;
};

export type Events = {
  toast: Toast;
};
