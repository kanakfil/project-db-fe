export type TimelineEvent = {
  status: string;
  date: string | null;
};
