export default class Project {
  constructor(
    id: number,
    name: string,
    statusHistory: [],
    type: string,
    model: LifecycleModel,
    roles: Role[]
  ) {
    this.id = id;
    this.name = name;
    this.statusHistory = statusHistory;
    this.type = type;
    this.model = model;
    this.status = this.statusHistory.at(-1)?.projectStatus;
    if (this.status != null) {
      this.progress =
        ((model.projectStatuses.indexOf(this.status) + 1) /
          model.projectStatuses.length) *
        100;
    } else {
      this.progress = 0;
    }
    this.roles = roles;
  }

  id: number;
  name: string;
  statusHistory: ProjectStatusHistoryItem[];
  type: string;
  roles: Role[];
  model: LifecycleModel;
  status: string | undefined;
  progress: number;
}

export class LifecycleModel {
  constructor(name: string, projectStatuses: string[]) {
    this.name = name;
    this.projectStatuses = projectStatuses;
  }

  name: string;
  projectStatuses: string[];
}

export class ProjectStatusHistoryItem {
  constructor(projectStatus: string, previous: string, date: string) {
    this.projectStatus = projectStatus;
    this.previous = previous;
    this.date = date;
  }

  projectStatus: string;
  previous: string;
  date: string;
}

export class Role {}
