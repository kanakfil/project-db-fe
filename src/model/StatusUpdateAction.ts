export default class StatusUpdateAction {
  constructor(from: string, to: string, moduleActions: ModuleAction[]) {
    this.from = from;
    this.to = to;
    this.moduleActions = moduleActions.map(
      (ma) => new ModuleAction(ma.module, ma.action)
    );
  }

  from: string;
  to: string;
  moduleActions: ModuleAction[];
}

export class ModuleAction {
  constructor(module: string, action: string) {
    this.module = module;
    this.action = action;
    this.name = this.module + ": " + this.action;
  }

  module: string;
  action: string;
  name: string;
}
