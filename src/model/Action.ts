import type Project from "@/model/Project";

export interface Action {
  token: string;
}
export interface ProjectAction extends Action {
  project: Project;
}
export interface ProjectUpdateAction extends ProjectAction {
  from: string;
  to: string;
  type: string;
}

export class ProjectUpdateStatusAction implements ProjectUpdateAction {
  constructor(from = "", token = "", project: Project, to = "") {
    this.from = from;
    this.token = token;
    this.project = project;
    this.to = to;
  }

  from: string;
  token: string;
  project: Project;
  to: string;
  type = "status";
}

export class ProjectUpdateTypeAction implements ProjectUpdateAction {
  constructor(from = "", token = "", project: Project, to = "") {
    this.from = from;
    this.token = token;
    this.project = project;
    this.to = to;
  }

  from: string;
  token: string;
  project: Project;
  to: string;
  type = "type";
}

export class ProjectUpdateModelAction implements ProjectUpdateAction {
  constructor(from = "", token = "", project: Project, to = "") {
    this.from = from;
    this.token = token;
    this.project = project;
    this.to = to;
  }

  from: string;
  token: string;
  project: Project;
  to: string;
  type = "model";
}
