import GitlabProject from "@/modules/vcs/gitlab/model/GitlabProject";

export default class ProjectService {
  getProject(response: any): GitlabProject {
    return new GitlabProject(
      response.id,
      response.name,
      response.lastActivityAt,
      response.since,
      response.commitCount
    );
  }

  getProjectDetails(response: any) {
    return response.map((r: any) => {
      return this.getProject(r);
    });
  }
}
