import { defineStore } from "pinia";
import { toastBus } from "@/main";
import axios from "axios";
import JiraProjectService from "../service/GitlabProjectService";
import type GitlabProject from "@/modules/vcs/gitlab/model/GitlabProject";

const SERVER_URL = import.meta.env["VITE_SERVER_URL"];
const client = axios.create({
  baseURL: SERVER_URL + "/api/modules/vcs/gitlab",
});

const projectService = new JiraProjectService();

export type GitlabProjectState = {
  projects: GitlabProject[];
  projectDetails: GitlabProject[];
  loading: boolean;
  error: unknown;
};

export const useGitlabProjectStore = defineStore({
  id: "gitlabproject",
  state: () =>
    ({
      projects: [],
      projectDetails: [],
      loading: false,
      error: null,
    } as GitlabProjectState),
  getters: {},
  actions: {
    async fetchProjects(token: string) {
      this.projects = [];
      this.loading = true;
      try {
        await client
          .get("/projects", {
            headers: {
              Authorization: "Bearer " + token,
            },
          })
          .then((response) => {
            this.projects = response.data.map((project: unknown) =>
              projectService.getProject(project)
            );

            this.projects.sort((it, sec) => it.name.localeCompare(sec.name));
            return this.projects;
          });
      } catch (error: any) {
        toastBus.emit("toast", {
          severity: "error",
          summary: error.name,
          detail: error.message,
        });
      } finally {
        this.loading = false;
      }
    },
    async getProjectDetails(id: number, token: string) {
      this.projectDetails = [];
      return client
        .get("/projects/" + id, {
          headers: {
            Authorization: "Bearer " + token,
          },
        })
        .then((response) => {
          this.projectDetails =
            projectService.getProjectDetails(response.data) ?? null;
          return response;
        });
    },
    async linkProject(id: number, token: string, gitlabId: string) {
      return client
        .put(
          "/projects/" + id,
          {
            gitlabId,
          },
          {
            headers: {
              Authorization: "Bearer " + token,
            },
          }
        )
        .then((response) => {
          return (this.projectDetails =
            projectService.getProjectDetails(response.data) ?? null);
        });
    },
  },
});
