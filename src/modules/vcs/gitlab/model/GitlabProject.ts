export default class GitlabProject {
  constructor(
    id: string,
    name: string,
    lastActivityAt: string,
    since: string,
    commitCount: number
  ) {
    this.id = id;
    this.name = name;
    this.lastActivityAt = lastActivityAt;
    this.since = since;
    this.commitCount = commitCount;
  }

  id: string;
  name: string;
  lastActivityAt: string;
  since: string;
  commitCount: number;
}
