import { defineStore } from "pinia";
import { toastBus } from "@/main";
import axios from "axios";
import type JiraProject from "@/modules/projectmanagement/jira/model/JiraProject";
import JiraProjectService from "../service/JiraProjectService";
import type { JiraProjectDetail } from "@/modules/projectmanagement/jira/model/JiraProject";

const SERVER_URL = import.meta.env["VITE_SERVER_URL"];
const client = axios.create({
  baseURL: SERVER_URL + "/api/modules/projectmanagement/jira",
});

const projectService = new JiraProjectService();

export type JiraProjectState = {
  projects: JiraProject[];
  projectDetails: JiraProjectDetail[];
  loading: boolean;
  error: unknown;
  totals: Record<string, number>;
};

export const useJiraProjectStore = defineStore({
  id: "jiraproject",
  state: () =>
    ({
      projects: [],
      projectDetails: [],
      loading: false,
      error: null,
      totals: {},
    } as JiraProjectState),
  getters: {},
  actions: {
    async fetchProjects(token: string) {
      this.projects = [];
      this.loading = true;
      try {
        await client
          .get("/projects", {
            headers: {
              Authorization: "Bearer " + token,
            },
          })
          .then((response) => {
            this.projects = response.data.map((project: unknown) =>
              projectService.getProject(project)
            );

            this.projects.sort((it, sec) => it.name.localeCompare(sec.name));
            return this.projects;
          });
      } catch (error: any) {
        toastBus.emit("toast", {
          severity: "error",
          summary: error.name,
          detail: error.message,
        });
      } finally {
        this.loading = false;
      }
    },
    async getProjectDetails(id: number, token: string) {
      this.projectDetails = [];
      return client
        .get("/projects/" + id, {
          headers: {
            Authorization: "Bearer " + token,
          },
        })
        .then((response) => {
          this.totals = response.data.totals;
          this.projectDetails =
            projectService.getProjectDetails(response.data) ?? null;
          return response;
        });
    },
    async linkProject(id: number, token: string, jiraId: string) {
      return client
        .put(
          "/projects/" + id,
          {
            jiraId,
          },
          {
            headers: {
              Authorization: "Bearer " + token,
            },
          }
        )
        .then((response) => {
          this.totals = response.data.totals;
          this.projectDetails =
            projectService.getProjectDetails(response.data) ?? null;
          return response;
        });
    },
  },
});
