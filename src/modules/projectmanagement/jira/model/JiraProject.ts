export default class JiraProject {
  constructor(id: string, key: string, name: string) {
    this.id = id;
    this.key = key;
    this.name = name;
  }

  id: string;
  key: string;
  name: string;
}

export class JiraProjectDetail extends JiraProject {
  constructor(
    id: string,
    key: string,
    name: string,
    epics: Epic[],
    worklogs: WorklogProjectDetail[]
  ) {
    super(id, key, name);
    this.epics = epics;
    this.worklogs = worklogs;
  }

  epics: Epic[];
  worklogs: WorklogProjectDetail[];
}
export class Epic {
  constructor(issueId: number, name: string, details: string) {
    this.issueId = issueId;
    this.name = name;
    this.details = details;
  }

  issueId: number;
  name: string;
  details: string;
}

export class WorklogProjectDetail {
  constructor(worker: string, amount: number, month: string) {
    this.worker = worker;
    this.amount = amount;
    this.month = month;
    this.amountMinutes = amount / 60;
  }

  worker: string;
  amount: number;
  month: string;
  amountMinutes: number;
}
