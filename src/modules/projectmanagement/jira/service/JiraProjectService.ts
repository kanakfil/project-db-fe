import Project, { LifecycleModel } from "@/model/Project";
import JiraProject, {
  Epic,
  JiraProjectDetail,
  WorklogProjectDetail,
} from "@/modules/projectmanagement/jira/model/JiraProject";

export default class ProjectService {
  getProject(response: any): JiraProject {
    return new JiraProject(response.id, response.key, response.name);
  }

  getProjectDetails(response: any) {
    return response.details.map((r: any) => {
      return new JiraProjectDetail(
        r.id,
        r.key,
        r.name,
        r.epics as Epic[],
        r.worklogs.map(
          (w: any) => new WorklogProjectDetail(w.worker, w.amount, w.month)
        )
      );
    });
  }
}
