import { defineConfig, loadEnv } from "vite";
import vue from "@vitejs/plugin-vue";
import { fileURLToPath } from "url";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  // envDir: "environment",

  // [vuestic-ui] Add alias for ~normalize.css.
  resolve: {
    alias: [
      { find: /^~(.*)$/, replacement: "$1" },
      {
        find: "@",
        replacement: fileURLToPath(new URL("./src", import.meta.url)),
      },
    ],
  },
  css: {
    preprocessorOptions: {
      scss: {
        additionalData: `@import "./src/assets/base";`,
      },
    },
  },
});
